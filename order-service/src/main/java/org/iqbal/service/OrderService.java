package org.iqbal.service;

import lombok.extern.slf4j.Slf4j;
import org.iqbal.entity.Order;
import org.iqbal.entity.OrderLineItems;
import org.iqbal.model.request.OrderLineItemsRequest;
import org.iqbal.model.request.OrderRequest;
import org.iqbal.model.response.Product;
import org.iqbal.model.response.client.ClientResponse;
import org.iqbal.model.response.InventoryResponse;
import org.iqbal.model.response.client.ClientUpdateProductResponse;
import org.iqbal.repository.OrderRepository;
import org.iqbal.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class OrderService {
    private final OrderRepository repository;
    private final ValidationUtils utils;
    private final WebClient webClient;
    private final String urlCheckStock;
    private final String updateProduct;

    @Autowired
    public OrderService(OrderRepository repository, ValidationUtils utils,
                        WebClient webClient,
                        @Value("${app.order-service.inventory-stock.url}") String urlCheckStock,
                        @Value("${app.order-service.update-product.url}") String updateProduct) {
        this.repository = repository;
        this.utils = utils;
        this.webClient = webClient;
        this.urlCheckStock = urlCheckStock;
        this.updateProduct = updateProduct;
    }

    @Transactional(rollbackFor = Exception.class)
    public String placeOrder(OrderRequest request) {

        utils.validate(request);

        List<OrderLineItems> orderLineItems = request.getOrderLineItemsRequests()
                .stream()
                .map(this::mapToDto)
                .toList();

        Order order = Order.builder()
                .orderNumber(UUID.randomUUID().toString())
                .orderLineItemsList(orderLineItems)
                .build();

        List<Long> inventoryId = order.getOrderLineItemsList().stream()
                .map(OrderLineItems::getInventoryId)
                .toList();

        ClientResponse inventoryResponse = getClientResponse(inventoryId);

        assert inventoryResponse != null;
        InventoryResponse[] responses = inventoryResponse.getData();

        int result = Arrays.stream(responses)
                .mapToInt(InventoryResponse::getStock)
                .sum();

        int totalOrderedQuantity = request.getOrderLineItemsRequests().stream()
                .mapToInt(OrderLineItemsRequest::getQuantity)
                .sum();

        List<Product> products = Arrays.stream(responses)
                .map(InventoryResponse::getProduct)
                .toList();

        for (InventoryResponse response : responses) {
            Integer orderedQuantity = request.getOrderLineItemsRequests().stream()
                    .filter(item -> item.getInventoryId().equals(response.getInventoryId()))
                    .mapToInt(OrderLineItemsRequest::getQuantity)
                    .sum();
            Product product = response.getProduct();
            product.setQuantity(product.getQuantity() - orderedQuantity);
        }

        if (totalOrderedQuantity <= result) {
            repository.save(order);

            List<Mono<ClientUpdateProductResponse>> updateRequests = products.stream()
                    .map(this::updateProductClient)
                    .toList();

            Flux.merge(updateRequests)
                    .doOnNext(responseEntity -> log.info("Status: {}", responseEntity.getStatus()))
                    .blockLast();
            return "Order Placed";
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Product is not in stock, please try again latter");
        }
    }

    private ClientResponse getClientResponse(List<Long> inventoryId) {
        return webClient.get()
                .uri(urlCheckStock,
                        uriBuilder -> uriBuilder.queryParam("id", inventoryId).build())
                .retrieve()
                .onStatus(status -> status.equals(HttpStatus.NOT_FOUND),
                        response -> Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Inventory Id Not Found")))
                .bodyToMono(ClientResponse.class)
                .block();
    }

    private Mono<ClientUpdateProductResponse> updateProductClient(Product product) {
        return webClient.put()
                .uri(updateProduct + product.getId())
                .body(Mono.just(product), Product.class)
                .retrieve()
                .onStatus(status -> status.equals(HttpStatus.NOT_FOUND),
                        response -> Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Product Id Not Found")))
                .bodyToMono(ClientUpdateProductResponse.class);
    }

    private OrderLineItems mapToDto(OrderLineItemsRequest request){
        return OrderLineItems.builder()
                .inventoryId(request.getInventoryId())
                .quantity(request.getQuantity())
                .build();
    }
}
