package org.iqbal.controller;

import lombok.RequiredArgsConstructor;
import org.iqbal.model.request.OrderRequest;
import org.iqbal.model.response.WebResponse;
import org.iqbal.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/orders")
public class OrderController {
    private final OrderService service;

    @PostMapping
    ResponseEntity<WebResponse<String>> placeOrder(@RequestBody OrderRequest request){
        String responses = service.placeOrder(request);

        WebResponse<String> response = WebResponse.<String>builder()
                .message("successfully")
                .status(HttpStatus.CREATED.getReasonPhrase())
                .data(responses)
                .build();

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }
}
