package org.iqbal.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderLineItemsRequest {
    @JsonIgnore
    private Long id;

    @NotNull(message = "Inventory ID not be null")
    @JsonProperty("inventory_id")
    private Long inventoryId;

    @Min(value = 1, message = "Quantity must be at least 1")
    private Integer quantity;
}
