package org.iqbal.model.response.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ClientUpdateProductResponse {
    private String message;
    private String status;
    private String data;
}
