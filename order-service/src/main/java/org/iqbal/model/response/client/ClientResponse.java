package org.iqbal.model.response.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.iqbal.model.response.InventoryResponse;

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Data
public class ClientResponse {
    private String message;
    private String status;
    private InventoryResponse[] data;
}
