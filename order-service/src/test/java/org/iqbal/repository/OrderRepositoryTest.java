package org.iqbal.repository;

import org.iqbal.entity.Order;
import org.iqbal.entity.OrderLineItems;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class OrderRepositoryTest {

    private final OrderRepository repository;

    @Autowired
    public OrderRepositoryTest(OrderRepository repository) {
        this.repository = repository;
    }

    @Test
    void create() {
        OrderLineItems item1 = new OrderLineItems(1L, 1L, 10);
        OrderLineItems item2 = new OrderLineItems(2L, 2L, 15);
        List<OrderLineItems> orderLineItemsList = new ArrayList<>();
        orderLineItemsList.add(item1);
        orderLineItemsList.add(item2);

        Order order = new Order();
        order.setId(1L);
        order.setOrderNumber("ORD-123");
        order.setOrderLineItemsList(orderLineItemsList);

        Order savedOrder = repository.save(order);
        Long savedOrderId = savedOrder.getId();

        Order retrievedOrder = repository.findById(savedOrderId).orElse(null);
        assertEquals(order.getOrderNumber(), Objects.requireNonNull(retrievedOrder).getOrderNumber());
        assertEquals(orderLineItemsList.size(), retrievedOrder.getOrderLineItemsList().size());
        assertEquals(orderLineItemsList.get(0).getId(), retrievedOrder.getOrderLineItemsList().get(0).getId());
        assertEquals(orderLineItemsList.get(1).getInventoryId(), retrievedOrder.getOrderLineItemsList().get(1).getInventoryId());
    }
}