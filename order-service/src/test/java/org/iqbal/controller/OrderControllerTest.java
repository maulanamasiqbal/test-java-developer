package org.iqbal.controller;

import org.iqbal.model.request.OrderRequest;
import org.iqbal.model.response.WebResponse;
import org.iqbal.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderControllerTest {

    @Mock
    private OrderService orderService;

    @InjectMocks
    private OrderController orderController;

    @Test
    void placeOrder() {

        OrderRequest request = new OrderRequest(/* provide necessary fields */);
        String expectedResponse = "Order placed successfully";

        when(orderService.placeOrder(any(OrderRequest.class))).thenReturn(expectedResponse);

        ResponseEntity<WebResponse<String>> responseEntity = orderController.placeOrder(request);
        WebResponse<String> responseBody = responseEntity.getBody();

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(HttpStatus.CREATED.getReasonPhrase(), Objects.requireNonNull(responseBody).getStatus());
        assertEquals("successfully", responseBody.getMessage());
        assertEquals(expectedResponse, responseBody.getData());
    }
}