package org.iqbal.exception;

import jakarta.validation.ConstraintViolationException;
import org.iqbal.model.response.WebResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice
public class ErrorController {

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<WebResponse<String>> handleResponseException(ResponseStatusException e){
        WebResponse<String> response = WebResponse.<String>builder()
                .message(e.getReason())
                .status(HttpStatus.valueOf(e.getStatusCode().value()).getReasonPhrase())
                .build();
        return ResponseEntity.status(e.getStatusCode()).body(response);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<WebResponse<String>> handleConstraintException(ConstraintViolationException e){
        WebResponse<String> response = WebResponse.<String>builder()
                .message(e.getMessage())
                .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }
}
