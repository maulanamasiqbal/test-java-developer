package org.iqbal.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class RestockRequest {
    @JsonIgnore
    private Long id;

    @Min(value = 1, message = "Quantity has be min value 1!")
    private Integer quantity;
}
