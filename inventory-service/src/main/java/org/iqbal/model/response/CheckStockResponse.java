package org.iqbal.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.iqbal.entity.Product;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CheckStockResponse {
    private Long inventoryId;
    private Integer stock;
    private Product product;
}
