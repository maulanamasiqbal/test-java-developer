package org.iqbal.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Data
@Builder
public class WebResponse<T> {

    private String message;

    private String status;

    private T data;
}
