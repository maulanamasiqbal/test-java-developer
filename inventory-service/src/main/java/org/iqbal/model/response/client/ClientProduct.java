package org.iqbal.model.response.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.iqbal.model.response.ProductResponse;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ClientProduct {
    private String message;
    private String status;
    private ProductResponse data;
}
