package org.iqbal.controller;

import lombok.RequiredArgsConstructor;
import org.iqbal.model.request.InventoryRequest;
import org.iqbal.model.response.CheckStockResponse;
import org.iqbal.model.response.InventoryResponse;
import org.iqbal.model.response.WebResponse;
import org.iqbal.service.InventoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/inventory")
@RequiredArgsConstructor
public class InventoryController {

    private final InventoryService service;

    @PostMapping
    ResponseEntity<WebResponse<InventoryResponse>> create(@RequestBody InventoryRequest request){
        InventoryResponse inventoryResponse = service.create(request);

        WebResponse<InventoryResponse> response = WebResponse.<InventoryResponse>builder()
                .message("successfully")
                .status(HttpStatus.CREATED.getReasonPhrase())
                .data(inventoryResponse)
                .build();

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping("/checkingStock")
    ResponseEntity<WebResponse<List<CheckStockResponse>>> checkingStock(@RequestParam("id")
                                                                  List<Long> id){
        List<CheckStockResponse> inventoryResponse = service.isInStock(id);

        WebResponse<List<CheckStockResponse>> response = WebResponse.<List<CheckStockResponse>>builder()
                .message("successfully")
                .status(HttpStatus.OK.getReasonPhrase())
                .data(inventoryResponse)
                .build();

        return ResponseEntity.ok(response);
    }
}
