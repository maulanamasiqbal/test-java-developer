package org.iqbal.controller;

import lombok.RequiredArgsConstructor;
import org.iqbal.entity.Product;
import org.iqbal.model.request.ProductRequest;
import org.iqbal.model.request.RestockRequest;
import org.iqbal.model.response.ProductResponse;
import org.iqbal.model.response.WebResponse;
import org.iqbal.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/products/")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService service;
    private static final String SUCCESS = "successfully";

    @PostMapping
    ResponseEntity<WebResponse<ProductResponse>> create(@RequestBody ProductRequest request){
        ProductResponse productResponse = service.create(request);

        WebResponse<ProductResponse> response = WebResponse.<ProductResponse>builder()
                .message(SUCCESS)
                .status(HttpStatus.CREATED.getReasonPhrase())
                .data(productResponse)
                .build();
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping("/{productId}/restocking")
    public ResponseEntity<WebResponse<ProductResponse>> restocking(@PathVariable("productId") Long id,
                                                                   @RequestBody RestockRequest request){
        request.setId(id);
        ProductResponse productResponse = service.restocking(request);

        WebResponse<ProductResponse> response = WebResponse.<ProductResponse>builder()
                .message(SUCCESS)
                .status(HttpStatus.OK.getReasonPhrase())
                .data(productResponse)
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<WebResponse<Integer>> checkQuantity(@PathVariable("productId") Long id){
        Integer check = service.checkQuantity(id);

        WebResponse<Integer> response = WebResponse.<Integer>builder()
                .message(SUCCESS)
                .status(HttpStatus.OK.getReasonPhrase())
                .data(check)
                .build();

        return ResponseEntity.ok(response);
    }

    @PutMapping("/{productId}")
    public ResponseEntity<WebResponse<String>> update(@PathVariable("productId") Long id,
                                                      @RequestBody Product product){
        product.setId(id);
        service.updateProduct(product);

        WebResponse<String> response = WebResponse.<String>builder()
                .message(SUCCESS)
                .status(HttpStatus.OK.getReasonPhrase())
                .data("OK")
                .build();

        return ResponseEntity.ok(response);
    }
}
