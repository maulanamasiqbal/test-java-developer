package org.iqbal.service;

import lombok.extern.slf4j.Slf4j;
import org.iqbal.entity.Product;
import org.iqbal.model.request.ProductRequest;
import org.iqbal.model.request.RestockRequest;
import org.iqbal.model.response.client.ClientProduct;
import org.iqbal.model.response.ProductResponse;
import org.iqbal.model.response.client.ClientUpdateProductResponse;
import org.iqbal.repository.ProductRepository;
import org.iqbal.utils.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
@Slf4j
public class ProductService {
    private final ProductRepository repository;
    private final ValidationUtils utils;
    private final WebClient webClient;
    private static final String NOT_FOUND = "Product Not Found";

    private final String url;

    @Autowired
    public ProductService(ProductRepository repository, ValidationUtils utils,
                          WebClient webClient, @Value("${app.inventory-service.uri}") String uri) {
        this.repository = repository;
        this.utils = utils;
        this.webClient = webClient;
        this.url = uri;
    }

    @Transactional(rollbackFor = Exception.class)
    public ProductResponse create(ProductRequest request){
        utils.validate(request);

        Product product = Product.builder()
                .name(request.getName())
                .price(request.getPrice())
                .quantity(request.getQuantity())
                .build();

        repository.saveAndFlush(product);

        createProduct(request)
                .subscribe(responseEntity -> log.info("Status Code: {}", responseEntity.getStatusCode()));

        return toProductResponse(product);
    }

    @Transactional(rollbackFor = Exception.class)
    public ProductResponse restocking(RestockRequest request){
        utils.validate(request);

        Product product = repository.findById(request.getId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, NOT_FOUND)
        );

        Integer restock = product.getQuantity() + request.getQuantity();
        product.setQuantity(restock);

        repository.save(product);

        restockingProduct(request)
                .subscribe(responseEntity -> log.info("Status: {}", responseEntity.getStatus()));

        return toProductResponse(product);
    }

    @Transactional(readOnly = true)
    public Integer checkQuantity(Long id){
        Product product = repository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, NOT_FOUND)
        );

        return product.getQuantity();
    }

    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Product get(Long id){
        return repository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, NOT_FOUND)
        );
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateProduct(Product product) {
        Optional<Product> byId = repository.findById(product.getId());

        if (byId.isEmpty()) throw  new ResponseStatusException(HttpStatus.NOT_FOUND, NOT_FOUND);

        repository.save(product);
        updateProductClient(product)
                .subscribe(responseEntity -> log.info("Status: {}", responseEntity.getStatus()));
    }

    private Mono<ResponseEntity<ClientProduct>> createProduct(ProductRequest request){
        return webClient.post()
                .uri(url)
                .body(Mono.just(request), ProductRequest.class)
                .retrieve()
                .toEntity(ClientProduct.class);
    }

    private Mono<ClientProduct> restockingProduct(RestockRequest request) {
        return webClient.put()
                .uri(url + request.getId() + "/restocking")
                .body(Mono.just(request), RestockRequest.class)
                .retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, this::handleClientError)
                .bodyToMono(ClientProduct.class);
    }

    private Mono<ClientUpdateProductResponse> updateProductClient(Product product) {
        return webClient.put()
                .uri(url + product.getId())
                .body(Mono.just(product), Product.class)
                .retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, this::handleClientError)
                .bodyToMono(ClientUpdateProductResponse.class);
    }

    private Mono<? extends Throwable> handleClientError(ClientResponse clientResponse) {
        return Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND,NOT_FOUND));
    }

    private ProductResponse toProductResponse(Product product){
        return ProductResponse.builder()
                .name(product.getName())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .build();
    }
}
