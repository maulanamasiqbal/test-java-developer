package org.iqbal.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.iqbal.entity.Inventory;
import org.iqbal.entity.Product;
import org.iqbal.model.request.InventoryRequest;
import org.iqbal.model.response.CheckStockResponse;
import org.iqbal.model.response.InventoryResponse;
import org.iqbal.model.response.ProductResponse;
import org.iqbal.repository.InventoryRepository;
import org.iqbal.utils.ValidationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class InventoryService {

    private final InventoryRepository repository;
    private final ProductService productService;
    private final ValidationUtils utils;

    @Transactional(rollbackFor = Exception.class, readOnly = true)
    public List<CheckStockResponse> isInStock(List<Long> id){
        List<Inventory> inventories = repository.findByIdIn(id);

        if (inventories.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product Not Found Id : " + id);

        return inventories.stream()
                .map(inventory ->
                        CheckStockResponse.builder()
                                .inventoryId(inventory.getId())
                                .stock(inventory.getProduct().getQuantity())
                                .product(inventory.getProduct())
                                .build()
                ).toList();
    }

    @Transactional(rollbackFor = Exception.class)
    public InventoryResponse create(InventoryRequest request) {
        utils.validate(request);

        Product product = productService.get(request.getProductId());

        Inventory inventory = Inventory.builder()
                .product(product)
                .build();

        repository.save(inventory);

        return toInventoryResponse(inventory);
    }

    private InventoryResponse toInventoryResponse(Inventory inventory){

        ProductResponse productResponse = ProductResponse.builder()
                .name(inventory.getProduct().getName())
                .price(inventory.getProduct().getPrice())
                .quantity(inventory.getProduct().getQuantity())
                .build();

        return InventoryResponse.builder()
                .id(inventory.getId())
                .products(productResponse)
                .build();
    }
}
