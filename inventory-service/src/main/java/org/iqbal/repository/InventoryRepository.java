package org.iqbal.repository;

import org.iqbal.entity.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InventoryRepository extends JpaRepository<Inventory, Long> {
    List<Inventory> findByIdIn(List<Long> id);
}
