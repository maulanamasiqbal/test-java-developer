package org.iqbal.repository;

import org.assertj.core.api.Assertions;
import org.iqbal.entity.Inventory;
import org.iqbal.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class InventoryRepositoryTest {

    private final InventoryRepository repository;
    private final ProductRepository productRepository;

    @Autowired
    public InventoryRepositoryTest(InventoryRepository repository, ProductRepository productRepository) {
        this.repository = repository;
        this.productRepository = productRepository;
    }

    @Test
    void save() {

        Product product = Product.builder()
                .id(3L)
                .build();
        productRepository.save(product);

        Inventory inventory = Inventory.builder()
                .id(1L)
                .product(product)
                .build();

        Inventory save = repository.save(inventory);

        Assertions.assertThat(save).isNotNull();
        Assertions.assertThat(save.getId()).isPositive();
    }

    @Test
    void findByIdIn() {

        Product product = Product.builder()
                .id(1L)
                .build();

        Product product2 = Product.builder()
                .id(2L)
                .build();
        productRepository.save(product);
        productRepository.save(product2);

        Inventory inventory1 = new Inventory(1L, product);
        Inventory inventory2 = new Inventory(2L, product2);

        // Save inventories to the database
        repository.saveAll(Arrays.asList(inventory1, inventory2));

        // When
        List<Inventory> foundInventories = repository.findByIdIn(Arrays.asList(1L, 2L));

        // Then
        assertEquals(2, foundInventories.size());
        assertEquals(inventory1.getId(), foundInventories.get(0).getId());
        assertEquals(inventory1.getProduct(), foundInventories.get(0).getProduct());
        assertEquals(inventory2.getId(), foundInventories.get(1).getId());
        assertEquals(inventory2.getProduct(), foundInventories.get(1).getProduct());
    }
}