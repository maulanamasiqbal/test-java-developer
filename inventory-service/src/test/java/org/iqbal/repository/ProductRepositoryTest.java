package org.iqbal.repository;

import org.assertj.core.api.Assertions;
import org.iqbal.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class ProductRepositoryTest {

    private final ProductRepository repository;

    @Autowired
    ProductRepositoryTest(ProductRepository repository) {
        this.repository = repository;
    }

    @Test
    void ProductRepository_SaveAll_ReturnProduct() {
        Product product = Product.builder()
                .name("Iphone 15")
                .price(BigDecimal.valueOf(2000))
                .quantity(10)
                .build();

        Product save = repository.saveAndFlush(product);

        Assertions.assertThat(save).isNotNull();
        Assertions.assertThat(save.getId()).isPositive();
    }

    @Test
    void findById() {

        Product product = Product.builder()
                .name("Iphone 14")
                .price(BigDecimal.valueOf(1000))
                .quantity(10)
                .build();

        repository.save(product);

        Product getId = repository.findById(product.getId()).orElseThrow();

        Assertions.assertThat(getId).isNotNull();
    }

    @Test
    void ProductRepository_Update_ReturnProduct() {

        Product product = Product.builder()
                .name("Iphone 12")
                .price(BigDecimal.valueOf(1000))
                .quantity(10)
                .build();

        repository.save(product);

        Product productSaved = repository.findById(product.getId()).orElseThrow();
        productSaved.setName("Iphone 13");
        productSaved.setQuantity(12);

        Product updated = repository.save(productSaved);

        Assertions.assertThat(updated.getName()).isNotNull();
        Assertions.assertThat(updated.getQuantity()).isNotNull();
        assertEquals(productSaved.getName(), updated.getName());
        assertEquals(productSaved.getQuantity(), updated.getQuantity());
    }
}