package org.iqbal.controller;

import org.iqbal.entity.Product;
import org.iqbal.model.request.ProductRequest;
import org.iqbal.model.request.RestockRequest;
import org.iqbal.model.response.ProductResponse;
import org.iqbal.model.response.WebResponse;
import org.iqbal.service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    @Test
    void create() {
        ProductRequest productRequest = new ProductRequest();
        productRequest.setName("Test Product");
        productRequest.setPrice(BigDecimal.valueOf(100.0));
        productRequest.setQuantity(10);

        ProductResponse expectedResponse = new ProductResponse();
        expectedResponse.setName(productRequest.getName());
        expectedResponse.setPrice(productRequest.getPrice());
        expectedResponse.setQuantity(productRequest.getQuantity());

        when(productService.create(any(ProductRequest.class))).thenReturn(expectedResponse);

        ResponseEntity<WebResponse<ProductResponse>> responseEntity = productController.create(productRequest);
        WebResponse<ProductResponse> responseBody = responseEntity.getBody();

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(HttpStatus.CREATED.getReasonPhrase(), Objects.requireNonNull(responseBody).getStatus());
        assertEquals("successfully", responseBody.getMessage());
        assertEquals(expectedResponse, responseBody.getData());
    }

    @Test
    void restocking() {

        Long productId = 1L;
        RestockRequest restockRequest = new RestockRequest();
        restockRequest.setId(productId);
        restockRequest.setQuantity(5);

        ProductResponse expectedResponse = new ProductResponse();
        expectedResponse.setName("Test Product");
        expectedResponse.setPrice(BigDecimal.valueOf(100.0));
        expectedResponse.setQuantity(15);

        when(productService.restocking(any(RestockRequest.class))).thenReturn(expectedResponse);

        ResponseEntity<WebResponse<ProductResponse>> responseEntity = productController.restocking(productId, restockRequest);
        WebResponse<ProductResponse> responseBody = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(HttpStatus.OK.getReasonPhrase(), Objects.requireNonNull(responseBody).getStatus());
        assertEquals("successfully", responseBody.getMessage());
        assertEquals(expectedResponse, responseBody.getData());
    }

    @Test
    void checkQuantity() {
        Long productId = 1L;
        Integer expectedQuantity = 10;

        when(productService.checkQuantity(productId)).thenReturn(expectedQuantity);

        ResponseEntity<WebResponse<Integer>> responseEntity = productController.checkQuantity(productId);
        WebResponse<Integer> responseBody = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(HttpStatus.OK.getReasonPhrase(), Objects.requireNonNull(responseBody).getStatus());
        assertEquals("successfully", responseBody.getMessage());
        assertEquals(expectedQuantity, responseBody.getData());
    }

    @Test
    void update() {
        Long productId = 1L;
        Product product = new Product();
        product.setId(productId);
        product.setName("Updated Product");
        product.setPrice(BigDecimal.valueOf(200.0));
        product.setQuantity(15);

        doNothing().when(productService).updateProduct(any(Product.class));

        ResponseEntity<WebResponse<String>> responseEntity = productController.update(productId, product);
        WebResponse<String> responseBody = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(HttpStatus.OK.getReasonPhrase(), Objects.requireNonNull(responseBody).getStatus());
        assertEquals("successfully", responseBody.getMessage());
        assertEquals("OK", responseBody.getData());
    }
}