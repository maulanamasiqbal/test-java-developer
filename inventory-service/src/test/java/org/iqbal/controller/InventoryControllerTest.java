package org.iqbal.controller;

import org.iqbal.entity.Product;
import org.iqbal.model.request.InventoryRequest;
import org.iqbal.model.response.CheckStockResponse;
import org.iqbal.model.response.InventoryResponse;
import org.iqbal.model.response.ProductResponse;
import org.iqbal.model.response.WebResponse;
import org.iqbal.service.InventoryService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InventoryControllerTest {

    @InjectMocks
    private InventoryController inventoryController;

    @Mock
    private InventoryService inventoryService;

    @Test
    void create() {
        InventoryRequest request = InventoryRequest.builder()
                .productId(1L)
                .build();

        ProductResponse productResponse = new ProductResponse("Product 1", BigDecimal.TEN, 10);
        when(inventoryService.create(any(InventoryRequest.class))).thenReturn(
                new InventoryResponse(1L, productResponse)
        );

        ResponseEntity<WebResponse<InventoryResponse>> responseEntity = inventoryController.create(request);

        // Then
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());

        WebResponse<InventoryResponse> responseBody = responseEntity.getBody();
        assertEquals("successfully", Objects.requireNonNull(responseBody).getMessage());
        assertEquals(HttpStatus.CREATED.getReasonPhrase(), responseBody.getStatus());

        InventoryResponse inventoryResponse = responseBody.getData();
        assertEquals(Long.valueOf(1L), inventoryResponse.getId());

        ProductResponse returnedProduct = inventoryResponse.getProducts();
        assertEquals("Product 1", returnedProduct.getName());
        assertEquals(BigDecimal.TEN, returnedProduct.getPrice());
        assertEquals(Integer.valueOf(10), returnedProduct.getQuantity());
        verify(inventoryService, times(1)).create(any(InventoryRequest.class));
    }

    @Test
    void checkingStock() {

        Product product = Product.builder()
                .name("testing1")
                .price(BigDecimal.valueOf(100))
                .quantity(10)
                .build();

        Product product2 = Product.builder()
                .name("testing2")
                .price(BigDecimal.valueOf(100))
                .quantity(10)
                .build();

        List<Long> ids = Arrays.asList(1L, 2L);

        CheckStockResponse response1 = new CheckStockResponse(1L, 10, product);
        CheckStockResponse response2 = new CheckStockResponse(2L, 10, product2);
        List<CheckStockResponse> inventoryResponses = Arrays.asList(response1, response2);

        when(inventoryService.isInStock(anyList())).thenReturn(inventoryResponses);

        ResponseEntity<WebResponse<List<CheckStockResponse>>> responseEntity = inventoryController.checkingStock(ids);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        WebResponse<List<CheckStockResponse>> responseBody = responseEntity.getBody();
        assertEquals("successfully", Objects.requireNonNull(responseBody).getMessage());
        assertEquals(HttpStatus.OK.getReasonPhrase(), responseBody.getStatus());

        List<CheckStockResponse> returnedResponses = responseBody.getData();
        assertEquals(2, returnedResponses.size());

        CheckStockResponse returnedResponse1 = returnedResponses.get(0);
        assertEquals(Long.valueOf(1L), returnedResponse1.getInventoryId());
        assertEquals("testing1", returnedResponse1.getProduct().getName());
        assertEquals(BigDecimal.valueOf(100), returnedResponse1.getProduct().getPrice());
        assertEquals(Integer.valueOf(10), returnedResponse1.getStock());

        CheckStockResponse returnedResponse2 = returnedResponses.get(1);
        assertEquals(Long.valueOf(2L), returnedResponse2.getInventoryId());
        assertEquals("testing2", returnedResponse2.getProduct().getName());
        assertEquals(BigDecimal.valueOf(100), returnedResponse2.getProduct().getPrice());
        assertEquals(Integer.valueOf(10), returnedResponse2.getStock());

        verify(inventoryService, times(1)).isInStock(ids);
    }
}