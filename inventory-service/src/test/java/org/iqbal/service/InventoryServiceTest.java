package org.iqbal.service;

import org.iqbal.entity.Inventory;
import org.iqbal.entity.Product;
import org.iqbal.model.request.InventoryRequest;
import org.iqbal.model.response.CheckStockResponse;
import org.iqbal.model.response.InventoryResponse;
import org.iqbal.repository.InventoryRepository;
import org.iqbal.utils.ValidationUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InventoryServiceTest {

    @Mock
    private ValidationUtils utils;

    @Mock
    private ProductService productService;

    @Mock
    private InventoryRepository inventoryRepository;

    @InjectMocks
    private InventoryService inventoryService;


    @Test
    void isInStock() {
        List<Long> ids = Arrays.asList(1L, 2L);

        Product product1 = new Product(1L, "Product 1", BigDecimal.TEN, 10);
        Product product2 = new Product(2L, "Product 2", BigDecimal.valueOf(15), 20);

        Inventory inventory1 = new Inventory(1L, product1);
        Inventory inventory2 = new Inventory(2L, product2);

        List<Inventory> inventories = Arrays.asList(inventory1, inventory2);

        when(inventoryRepository.findByIdIn(anyList())).thenReturn(inventories);

        List<CheckStockResponse> responses = inventoryService.isInStock(ids);

        assertEquals(2, responses.size());

        CheckStockResponse response1 = responses.get(0);
        assertEquals(inventory1.getId(), response1.getInventoryId());
        assertEquals(product1.getQuantity(), response1.getStock());
        assertEquals(product1, response1.getProduct());

        CheckStockResponse response2 = responses.get(1);
        assertEquals(inventory2.getId(), response2.getInventoryId());
        assertEquals(product2.getQuantity(), response2.getStock());
        assertEquals(product2, response2.getProduct());

        verify(inventoryRepository, times(1)).findByIdIn(ids);
    }

    @Test
    void create() {

        InventoryRequest request = new InventoryRequest(1L);

        Product product = Product.builder()
                .id(1L)
                .name("testing123")
                .build();
        Inventory inventory = new Inventory(1L, product);

        doNothing().when(utils).validate(request);

        when(productService.get(request.getProductId())).thenReturn(product);
        when(inventoryRepository.save(any(Inventory.class))).thenReturn(inventory);

        InventoryResponse response = inventoryService.create(request);

        assertEquals(product.getName(), response.getProducts().getName());

        verify(utils, times(1)).validate(request);
        verify(productService, times(1)).get(request.getProductId());
        verify(inventoryRepository, times(1)).save(any(Inventory.class));
    }
}