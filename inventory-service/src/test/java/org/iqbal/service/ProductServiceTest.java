package org.iqbal.service;

import org.iqbal.entity.Product;
import org.iqbal.model.request.ProductRequest;
import org.iqbal.model.request.RestockRequest;
import org.iqbal.model.response.ProductResponse;
import org.iqbal.model.response.client.ClientProduct;
import org.iqbal.repository.ProductRepository;
import org.iqbal.utils.ValidationUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository repository;

    @Mock
    private ValidationUtils utils;

    @InjectMocks
    private ProductService productService;

    @Test
    void checkQuantity() {
        Long productId = 1L;
        Integer expectedQuantity = 10;
        Product product = new Product();
        product.setId(productId);
        product.setQuantity(expectedQuantity);

        when(repository.findById(productId)).thenReturn(Optional.of(product));

        Integer actualQuantity = productService.checkQuantity(productId);

        assertEquals(expectedQuantity, actualQuantity);
    }

    @Test
    void get() {
        Long productId = 1L;
        Product expectedProduct = new Product();
        expectedProduct.setId(productId);
        expectedProduct.setName("Test Product");

        when(repository.findById(productId)).thenReturn(Optional.of(expectedProduct));

        Product actualProduct = productService.get(productId);

        assertEquals(expectedProduct.getId(), actualProduct.getId());
        assertEquals(expectedProduct.getName(), actualProduct.getName());
    }
}