package org.iqbal.model.request;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ProductRequest {

    @NotEmpty(message = "Product Name not has be empty!")
    private String name;

    @DecimalMin(value = "100.00", message = "Price must be at least 100.00")
    private BigDecimal price;

    @Min(value = 10, message = "Quantity has be minimal 10!")
    private Integer quantity;
}