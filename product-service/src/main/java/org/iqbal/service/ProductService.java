package org.iqbal.service;

import lombok.RequiredArgsConstructor;
import org.iqbal.entity.Product;
import org.iqbal.model.request.ProductRequest;
import org.iqbal.model.request.RestockRequest;
import org.iqbal.model.response.ProductResponse;
import org.iqbal.repository.ProductRepository;
import org.iqbal.utils.ValidationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository repository;

    private final ValidationUtils utils;

    @Transactional(rollbackFor = Exception.class)
    public ProductResponse create(ProductRequest request){
        utils.validate(request);

        Product product = Product.builder()
                .name(request.getName())
                .price(request.getPrice())
                .quantity(request.getQuantity())
                .build();

        repository.saveAndFlush(product);

        return toProductResponse(product);
    }

    @Transactional(readOnly = true)
    public List<ProductResponse> getAll(){
        List<Product> all = repository.findAll();

        return all.stream().map(this::toProductResponse).toList();
    }

    @Transactional(rollbackFor = Exception.class)
    public ProductResponse restock(RestockRequest request) {
        utils.validate(request);

        Product product = repository.findById(request.getId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product Not Found")
        );

        Integer quantityNow = product.getQuantity() + request.getQuantity();
        product.setQuantity(quantityNow);

        repository.save(product);

        return toProductResponse(product);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateProduct(Product product) {
        Optional<Product> byId = repository.findById(product.getId());

        if (byId.isEmpty()) throw  new ResponseStatusException(HttpStatus.NOT_FOUND, "Product Not Found");

        repository.save(product);
    }

    private ProductResponse toProductResponse(Product product){
        return ProductResponse.builder()
                .name(product.getName())
                .price(product.getPrice())
                .quantity(product.getQuantity())
                .build();
    }
}
