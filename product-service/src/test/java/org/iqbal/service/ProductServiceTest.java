package org.iqbal.service;

import org.iqbal.entity.Product;
import org.iqbal.model.request.ProductRequest;
import org.iqbal.model.request.RestockRequest;
import org.iqbal.model.response.ProductResponse;
import org.iqbal.repository.ProductRepository;
import org.iqbal.utils.ValidationUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository repository;

    @Mock
    private ValidationUtils utils;

    @Test
    void create() {

        ProductRequest request = ProductRequest.builder()
                .name("Iphone 15")
                .price(BigDecimal.valueOf(2000))
                .quantity(10)
                .build();

        Product product = Product.builder()
                .name(request.getName())
                .price(request.getPrice())
                .quantity(request.getQuantity())
                .build();

        when(repository.saveAndFlush(any(Product.class))).thenReturn(product);

        ProductResponse productResponse = productService.create(request);

        verify(utils).validate(request);

        assertEquals(product.getName(), productResponse.getName());
        assertEquals(product.getPrice(), productResponse.getPrice());
    }

    @Test
    void getAll() {

        List<Product> dummyProducts = new ArrayList<>();
        dummyProducts.add(new Product(1L, "Product 1", BigDecimal.valueOf(2000), 10));
        dummyProducts.add(new Product(2L, "Product 2",  BigDecimal.valueOf(1000), 20));

        when(repository.findAll()).thenReturn(dummyProducts);

        List<ProductResponse> result = productService.getAll();

        assertEquals(dummyProducts.size(), result.size());
        for (int i = 0; i < dummyProducts.size(); i++) {
            assertEquals(dummyProducts.get(i).getName(), result.get(i).getName());
            assertEquals(dummyProducts.get(i).getPrice(), result.get(i).getPrice());
        }
    }

    @Test
    void restock() {
        RestockRequest request = RestockRequest.builder()
                .id(1L)
                .quantity(12)
                .build();

        Product product = Product.builder()
                .id(1L)
                .name("Iphone")
                .price(BigDecimal.valueOf(1000))
                .quantity(20)
                .build();

        when(repository.findById(1L)).thenReturn(Optional.of(product));

        // When
        ProductResponse response = productService.restock(request);

        verify(utils).validate(request);

        // Then
        assertEquals(product.getPrice(), response.getPrice());
        assertEquals(product.getName(), response.getName());
        assertEquals(product.getQuantity(), response.getQuantity());

        verify(repository, times(1)).findById(1L);
        verify(repository, times(1)).save(product);
    }

    @Test
    void updateProduct() {

        Product product = Product.builder()
                .id(1L)
                .name("Iphone")
                .price(BigDecimal.valueOf(1000))
                .quantity(20)
                .build();

        Optional<Product> existingProduct = Optional.of(new Product(1L, "Existing Product",
                BigDecimal.valueOf(1000),5));

        when(repository.findById(1L)).thenReturn(existingProduct);

        // When
        productService.updateProduct(product);

        // Then
        verify(repository, times(1)).findById(1L);
        verify(repository, times(1)).save(product);
    }
}