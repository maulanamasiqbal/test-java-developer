package org.iqbal.controller;

import org.iqbal.entity.Product;
import org.iqbal.model.request.ProductRequest;
import org.iqbal.model.request.RestockRequest;
import org.iqbal.model.response.ProductResponse;
import org.iqbal.model.response.WebResponse;
import org.iqbal.service.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    @Test
    void create() {

        ProductRequest request = ProductRequest.builder()
                .name("Iphone 15")
                .price(BigDecimal.valueOf(2000))
                .quantity(10)
                .build();

        ProductResponse expectedResponse = ProductResponse.builder()
                .name("Iphone 15")
                .price(BigDecimal.valueOf(2000))
                .quantity(10)
                .build();

        when(productService.create(request)).thenReturn(expectedResponse);

        ResponseEntity<WebResponse<ProductResponse>> responseEntity = productController.create(request);

        verify(productService, times(1)).create(request);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        WebResponse<ProductResponse> responseBody = responseEntity.getBody();
        assertEquals("successfully create", Objects.requireNonNull(responseBody).getMessage());
        assertEquals(expectedResponse, responseBody.getData());
    }

    @Test
    void getAll() {

        List<ProductResponse> dummyProducts = new ArrayList<>();
        dummyProducts.add(new ProductResponse( "Product 1", BigDecimal.valueOf(2000), 10));
        dummyProducts.add(new ProductResponse( "Product 2", BigDecimal.valueOf(1000), 20));

        when(productService.getAll()).thenReturn(dummyProducts);

        ResponseEntity<WebResponse<List<ProductResponse>>> responseEntity = productController.getAll();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        WebResponse<List<ProductResponse>> responseBody = responseEntity.getBody();
        assertEquals("successfully", Objects.requireNonNull(responseBody).getMessage());
        assertEquals(dummyProducts, responseBody.getData());
    }

    @Test
    void restocking() {

        Long productId = 1L;

        RestockRequest request = RestockRequest.builder()
                .id(1L)
                .quantity(10)
                .build();

        ProductResponse expectedResponse = ProductResponse.builder()
                .name("Iphone 15")
                .price(BigDecimal.valueOf(2000))
                .quantity(20)
                .build();

        when(productService.restock(request)).thenReturn(expectedResponse);

        ResponseEntity<WebResponse<ProductResponse>> responseEntity = productController.restocking(productId, request);

        verify(productService, times(1)).restock(request);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        WebResponse<ProductResponse> responseBody = responseEntity.getBody();
        assertEquals("successfully", Objects.requireNonNull(responseBody).getMessage());
        assertEquals(expectedResponse, responseBody.getData());
    }

    @Test
    void update() {

        Long productId = 1L;
        Product product = Product.builder()
                .id(1L)
                .name("Testing1")
                .price(BigDecimal.valueOf(1000))
                .quantity(100)
                .build();

        ResponseEntity<WebResponse<String>> responseEntity = productController.update(productId, product);

        verify(productService, times(1)).updateProduct(product);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        WebResponse<String> responseBody = responseEntity.getBody();
        assertEquals("successfully", Objects.requireNonNull(responseBody).getMessage());
        assertEquals("OK", responseBody.getData());
    }
}